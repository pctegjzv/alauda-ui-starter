# Alauda Ui Starter

![baby mario](baby-mario.png)

这是 Alauda UI 全栈脚手架。你可以通过此项目启动任何的 Alauda UI 的 Angular + Go 项目。

此项目包含了目前 Alauda 工程师总结的前端和后端最佳实践。在开发过程中，假如你有任何工程化上的新想法和建议，可以以此项目作为最小颗粒度的试验田，并在实践成熟后，通过 PR merge 进 master 分支。

# 特性

* Golang
* Angular CLI
* Yarn 作为包管理器
* Jest 作为 UI test runner
  * 包含示例测试用例，具体用法请参考 Jest 官方文档
* 按需加载模块示例
* 国际化翻译
* 基于 Protractor 的 e2e 测试

# Getting Started

## 准备

请确保以下依赖已安装，并且正确配置了 `$PATH` 环境变量。

* Golang 1.8+
* Node.js 8+
* Yarn 1.3+
* UPX 3.9+。可选，用于生产环境构建 (Mac 下安装可使用[Homebrew](http://brewinstall.org/Install-upx-on-Mac-with-Brew/))

> 注意，由于 Go 语言对于项目管理的机制，**本地开发时请将 alauda-ui-starter 放到$GOPATH/src 下，默认为${HOME}/go/src**
>
> 具体原因可参考[这里](https://groups.google.com/forum/#!topic/golang-nuts/6orXabMNivE)

## 配置本地开发环境

请在开发 frontend/e2e/backend 前，首先执行`yarn install`。
安装之后执行 `yarn start`，启动UI Starter。

### e2e

### 本地环境

```
# 启动 Starter UI App
yarn start

# 安装ChromeDriver
yarn e2e:update-webdriver-alternative

# 运行E2E测试
yarn e2e:test-only
```

### Headless（一般用于无桌面环境的 CI 环境）

```
yarn e2e:integration
```

### Golang backend 构建

```
yarn run build:backend
```

## 国际化翻译

Starter 提供了一个按模块加载翻译文件的方案，具体方式是：

* `TranslateModule`提供公共的翻译文件，放在`translate/i18n.ts`里，在 App 初始化时载入到 App 中。
* `AppModule`使用`TranslateModule.forRoot`提供`TranslateService`给全局使用
* `ShareModule`导出跟翻译有关的 pipe 和 directive，供 feature 模块使用
* feature 模块（可参考`DemoFeatureModule`）的构造函数使用`translate.setTranslations(modulePrefix, i18n)`载入到全局。modulePrefix 会作为前缀加到业务模块的翻译 key 上。

也就是说，全局的翻译文件内容是通过一个全局服务提供给不同业务模块的，但每个业务模块的翻译文件将跟着自己模块的初始化进行加载。

### TODO

按需加载不同 language 的语言 assets

### 注

* 尽量不要在代码里再使用`translate.get`方法，而是转为`translate.stream`，这样切换语言时可以不用刷新页面，也就提供更优雅的语言切换方式。
* i18n 文件是普通的 TypeScript 文件，并且需要同时提供多种语言的翻译，这样考虑是：
* 多语言放到一个文件后，由于大部分的翻译文件是随着模块异步加载的，所以并不会显著增加初始翻译文件的大小
* 现在 Angular CLI 不支持自定义 Webpack loader，导致按模块的翻译文件不能通过比较优雅的方式异步载入
    * 假设支持自定义 loader 后，我们可以通过 loader 来拆分一个文件内的多语言内容
* 放到同一个文件时，比较容易交给专业人员进行翻译
* 本地开发友好
* 使用 TypeScript 后，能比较容易做一些代码级别的翻译字符串生成
* modulePrefix 会加到每个翻译的 key 作为前缀，比较松散的方式提供翻译的命名空间实现
    * 也就是说，在使用业务模块翻译时，应手动添加模块的 prefix
* 不同的业务模块之间不能相互使用翻译文件。假如需要共享翻译字符串，则需要放到公共的翻译文件中

## SVG 图标

图标实现目前参考了 Material 的 MatIcon 实现。使用自定义的 SVG 图标需要三步：

1. 在`src/assets/icons`添加你的 svg 图片，比如`my-icon.svg`
2. 在`src/app/services/icon.service.ts`的`SvgIcons`添加`"my-icon" => "my-icon.svg"`
3. 在模板内使用`<mat-icon svgIcon="my-icon"></my-icon>`展示 SVG 图标请参考https://material.angular.io/components/icon/overview#svg-icons
   关于 SVG icon 的内容。

### TODO

* 自动化生成 SvgIcons 映射
* 自己封装 Icon 组件，用同一个接口对应 SVG/CSS/Font 图标

# 额外配置

## 更改 Angular prefix

你可以通过以下命令修改 Starter 的默认 prefix ( 比如
`app-some-component`、`appSomeDirective`) 。修改完后，请修复项目的 lint 问题。

```
ng set 'apps[0].prefix'='your-prefix'
```

## 单独更新 alauda-ui 依赖

以下命令会更新alauda-ui至最新代码，并且更新`yarn.lock`文件。
```
yarn upgrade alauda-ui 
```
