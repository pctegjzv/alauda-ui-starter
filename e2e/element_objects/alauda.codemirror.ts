/**
 * 编辑YAML 的控件
 * Created by liuwei on 2018/2/22.
 */
import {browser,element} from 'protractor';

export class AlaudaCodemirror {
  public codemirror;

  constructor(selector) {
    this.codemirror = element(selector);
  }

  /**
   * 输入yaml 的值
   *
   * @parameter {yamlcontent} yaml 的值
   */
  input(yamlContent) {
    this.codemirror.click();
    browser.actions().sendKeys(yamlContent).perform();
    return browser.sleep(500);
  }

}
