/**
 * 下拉框列表，由一个检索框和下拉框组成
 * Created by liuwei on 2018/2/22.
 */
import {element, protractor} from 'protractor';
import {AlaudaInputbox} from '../element_objects/alauda.inputbox';
import {AlaudaButton} from '../element_objects/alauda.button';
import {CommonPage} from '../utility/common.page';

export class AlaudaDropdownlist {
  public alauda_inputBox;
  public itemlist;
  public alauda_buttonIcon;

  constructor(inputboxselector, iconselector, itemlistselector) {
    this.alauda_inputBox = new AlaudaInputbox(inputboxselector);
    this.itemlist = element(itemlistselector);
    this.alauda_buttonIcon = new AlaudaButton(iconselector);
  }

  /**
   * 单击下拉框，在检索框中输入要选择的值，单击ENTER键选择该值
   *
   * @parameter {selectvalue} 从下拉框中要选择的值
   */
  search(selectvalue) {
    this.alauda_buttonIcon.click();
    CommonPage.waitElementPresent(this.alauda_divlist);
    this.alauda_inputBox.input(selectvalue);
    this.alauda_inputBox.inputBox.sendKeys(protractor.Key.ENTER);

    this.itemlist.isPresent().then(function (isPresent) {
      if (isPresent) {
        this.itemlist.isDisplayed().then(function (isDisplayed) {
          if (isDisplayed) {
            this.alauda_buttonIcon.click();
          }
        })
      }
    })

  }

}

