/**
 * 确认对话框，包含确认提示内容，取消按钮，确认按钮
 * Created by liuwei on 2018/2/22.
 */

import {element} from 'protractor';
import {AlaudaButton} from '../element_objects/alauda.button';

export class AlaudaConfirmDialog {
  public label_content;
  public alauda_cancelbutton;
  public alauda_confirmbutton;

  /**
   * 确认对话框的构造函数
   *
   * @parameter {content_selector} 提示内容的选择器
   * @parameter {buttoncancel_selector} 取消按钮的选择器
   * @parameter {buttonconfirm_selector} 确认按钮的选择器
   */
  constructor(content_selector, buttoncancel_selector, buttonconfirm_selector) {
    this.label_content = element(content_selector);
    this.alauda_cancelbutton = new AlaudaButton(buttoncancel_selector);
    this.alauda_confirmbutton = new AlaudaButton(buttonconfirm_selector);
  }

  /**
   * 单击确认按钮
   *
   */
  confirm() {
    this.alauda_confirmbutton.click();
  }

  /**
   * 单击取消按钮
   *
   */
  cancel() {
    this.alauda_cancelbutton.click();
  }

}

