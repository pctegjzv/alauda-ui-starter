/**
 * 按钮控件
 * Created by liuwei on 2018/2/22.
 */


import {browser, element} from 'protractor';

export class AlaudaButton {
  public button;

  constructor(selector) {
    this.button = element(selector);
  }

  /**
   * 单击按钮
   *
   */
  click() {
    this.button.click();
    return browser.sleep(500);
  }
}





