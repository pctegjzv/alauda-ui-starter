/**
 * Created by liuwei on 2018/2/14.
 */
import {browser, by, element} from 'protractor';

export class CommonPage {

  /**
   * wait an element to present on the page
   *
   * @parameter {element} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementPresent(element, timeout = 120000) {
    return browser.driver.wait(() => {
      return element.isPresent().then((isPresent)=>isPresent)
    }, timeout).then(() => true, (err) => {
      console.warn('wait error [' + err + ']');
      return false;
    })
  }

  /**
   * wait an element to disappear on the page
   *
   * @parameter {element} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotPresent(element, timeout = 120000) {
    return browser.driver.wait(() => {
      return element.isPresent().then((isPresent) => !isPresent)
    }, timeout).then(() => true, (err) => {
      console.warn('wait error [' + err + ']');
      return false;
    })
  }

  /**
   * wait an element to display on the page
   *
   * @parameter {element} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementDisplay(element, timeout = 120000) {
    return browser.driver.wait(() => {
      return element.isDisplayed().then((isDisplayed) => isDisplayed)
    }, timeout).then(() => true, (err) => {
      console.warn('wait error: [' + err + ']');
      return false;
    })
  }

  /**
   * wait an element to hidden on the page
   *
   * @parameter {element} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotDisplay(element, timeout = 120000) {
    return browser.driver.wait(() => {
      return element.isDisplayed().then((isDisplayed) => !isDisplayed)
    }, timeout).then(() => true, (err) => {
      console.warn('wait error: [' + err + ']');
      return false;
    })
  }
}
