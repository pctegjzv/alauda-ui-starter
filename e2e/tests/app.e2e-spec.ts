import { AppPage } from '../page_objects/app.po';

describe('alauda-ui-starter App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('Alauda UI stater');
  });
});
