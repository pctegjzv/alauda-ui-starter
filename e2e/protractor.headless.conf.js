const { config } = require('./protractor.conf');

exports.config = Object.assign({}, config, {
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['--headless', '--disable-gpu', '--window-size=800,600', '--no-sandbox'],
    }
  },
});
