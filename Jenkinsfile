// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
// image can be used for promoting...
def IMAGE
def CURRENT_VERSION
def code_data
def DEBUG = false
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }
    //(optional) 环境变量
    environment {
      FOLDER = '$GOPATH/src/alauda-ui-starter'

      // for building an scanning
      REPOSITORY = "alauda-ui-starter"
      OWNER = "mathildetech"
      // sonar feedback user
      // needs to change together with the credentialsID
      BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
      SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
      NAMESPACE = "alauda-system"
      DEPLOYMENT = "starter-backend"
      CONTAINER = "backend"
    }
    // stages
    stages {
      stage('Checkout') {
        steps {
          script {
              // checkout code
              def scmVars = checkout scm
              // extract git information
              env.GIT_COMMIT = scmVars.GIT_COMMIT
              env.GIT_BRANCH = scmVars.GIT_BRANCH
              GIT_COMMIT = "${scmVars.GIT_COMMIT}"
              GIT_BRANCH = "${scmVars.GIT_BRANCH}"
          }
          // moving project code for the specified folder
          sh """
              mkdir -p ${FOLDER}
              cp -R . ${FOLDER}
              cp -R .git ${FOLDER}/.git
          """
          // installing golang coverage and report tools
          sh """
              go get -u gopkg.in/alecthomas/gometalinter.v2
              gometalinter.v2 --install
              go get -u github.com/axw/gocov/...
              go get -u github.com/AlekSi/gocov-xml
              go get -u github.com/jstemmer/go-junit-report
          """
        }
      }
      stage('Build UI') {
        steps {
          script {
            sshagent(credentials: ['alauda-ui-install']){
                sh "cd ${FOLDER} && export GIT_SSH=./ssh-bitbucket.sh && chmod +x ./ssh-bitbucket.sh && yarn install && yarn build"
            }
          }
        }
      }
      stage('CI'){
        failFast true
        parallel {
          stage('Build') {
              steps {
                  script {
                    // backend build
                    // TODO: optmize docker build
                    // for some reason the below is not working
                    // dir("${FOLDER}/src/backend") {
                    //   sh "make build"
                    //   sh "cp images/Dockerfile dist/Dockerfile"
                    // }
                    // the built go app is not recognized by alpine
                    // once starts working the docker build parameters needs to be changed

                    // currently is building code inside the container
                    IMAGE = deploy.dockerBuild(
                        "${FOLDER}/src/backend/images/Dockerfile.build", //Dockerfile
                        "${FOLDER}", // build context
                        "index.alauda.cn/alaudak8s/starter-ui", // repo address
                        "${GIT_COMMIT}", // tag
                        "alaudak8s", // credentials for pushing
                    )
                    // start and push
                    IMAGE.start().push()
                  }

              }
          }
          stage('Code Scan') {
            steps {
                script {
                  // ui tests
                  // skipping ui test
                  // sh "cd ${FOLDER} && yarn run test:ci"

                  // backend
                  // generate reports
                  sh "cd ${FOLDER}/src/backend && gometalinter.v2 --checkstyle --deadline 10m --skip=vendor ./src/backend/...  > report.xml || true"
                  sh "cd ${FOLDER}/src/backend && gocov test `go list ./... | grep -v /vendor/` | gocov-xml > coverage.xml"
                  sh "cd ${FOLDER}/src/backend && go test -v `go list ./... | grep -v /vendor/` | go-junit-report > test.xml"

                  // scanning
                  deploy.scan(
                    REPOSITORY,
                    GIT_BRANCH,
                    SONARQUBE_BITBUCKET_CREDENTIALS,
                    FOLDER,
                    DEBUG,
                    OWNER,
                    BITBUCKET_FEEDBACK_ACCOUNT).start()
                }
            }
          }
        }
      }
      // after build it should start deploying
      stage('Deploying') {
          steps {
              echo "here we can start to deploy"
              script {
                  // setup kubectl
                  if (GIT_BRANCH == "master") {
                      // master is already merged
                      deploy.setupStaging()

                  } else {
                      // pull-requests
                      deploy.setupInt()
                  }
                  // saving current state
                  CURRENT_VERSION = deploy.getDeployment(NAMESPACE, DEPLOYMENT)
                  // starts deploying
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
              }
          }
      }
      stage('Test') {
          // skipping because of private repository
          // TODO: Once the ui build is ready and it can send requests
          // to a remote server we can enable this
          when {
              expression {
                false
                  // GIT_BRANCH == "master"
              }
          }
          // UI automation
          agent { label 'chrome' }

          steps {
              checkout scm
              sh "yarn"
              sh "yarn e2e:integration"
          }
      }
      stage('Manual test') {
          when {
              expression {
                GIT_BRANCH == "master"
              }
          }
          steps {
              input message: 'Ready?'
          }
      }
      stage('Promoting') {
          // limit this stage to master only
          when {
              expression {
                  GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                  // setup kubectl
                  deploy.setupProd()

                  // promote to release
                  IMAGE.push("release")

                  // update production environment
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
              }
          }
      }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
          echo "Horay!"
        }

        // 失败
        failure {
            echo "damn!"
            // check the npm log
            // fails lets check if it
            script {
                if (CURRENT_VERSION != null) {
                  if (GIT_BRANCH == "master") {
                    deploy.setupStaging()
                  } else {
                    deploy.setupInt()
                  }
                    deploy.rollbackDeployment(
                        NAMESPACE,
                        DEPLOYMENT,
                        CONTAINER,
                        CURRENT_VERSION,
                    )
                }
            }
        }
        // 取消的
        aborted {
          echo "aborted!"
          script {
                if (CURRENT_VERSION != null) {
                  if (GIT_BRANCH == "master") {
                    deploy.setupStaging()
                  } else {
                    deploy.setupInt()
                  }
                    deploy.rollbackDeployment(
                        NAMESPACE,
                        DEPLOYMENT,
                        CONTAINER,
                        CURRENT_VERSION,
                    )
                }
            }
        }
        // finally
        always {
          echo "always!"
        }
    }
}
