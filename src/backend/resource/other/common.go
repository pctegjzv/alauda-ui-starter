package other

import (
	"alauda-ui-starter/src/backend/api"
	"alauda-ui-starter/src/backend/resource/dataselect"
)

var KindToName = map[string]string{}

type FieldPayload map[string]string

const (
	NamespacedScope = "namespaced"
	ClusteredScope  = "clustered"
)

type ResourceList struct {
	ListMeta  api.ListMeta    `json:"listMeta"`
	Resources []*ResourceMeta `json:"resources"`
	Errors    []error         `json:"errors"`
}

type ResourceMeta struct {
	ObjectMeta api.ObjectMeta   `json:"objectMeta"`
	TypeMeta   ResourceTypeMeta `json:"typeMeta"`
	Scope      string           `json:"scope"`
}

type ResourceTypeMeta struct {
	Name         string `json:"name"`
	Kind         string `json:"kind"`
	GroupVersion string `json:"groupVersion"`
}

func (r *ResourceMeta) GetProperty(name dataselect.PropertyName) dataselect.ComparableValue {
	switch name {
	case dataselect.NameProperty:
		return dataselect.StdComparableString(r.ObjectMeta.Name)
	case dataselect.NameLengthProperty:
		return dataselect.StdComparableInt(len(r.ObjectMeta.Name))
	case dataselect.CreationTimestampProperty:
		return dataselect.StdComparableTime(r.ObjectMeta.CreationTimestamp.Time)
	case dataselect.NamespaceProperty:
		return dataselect.StdComparableString(r.ObjectMeta.Namespace)
	case dataselect.ScopeProperty:
		return dataselect.StdComparableString(r.Scope)
	case dataselect.KindProperty:
		return dataselect.StdComparableString(r.TypeMeta.Kind)
	default:
		return nil
	}
}

func (r *ResourceMeta) setScope() {
	if r.ObjectMeta.Namespace == "" {
		r.Scope = ClusteredScope
	} else {
		r.Scope = NamespacedScope
	}
}

func toCells(std []*ResourceMeta) []dataselect.DataCell {
	cells := make([]dataselect.DataCell, len(std))
	for i := range std {
		cells[i] = std[i]
	}
	return cells
}

func fromCells(cells []dataselect.DataCell) []*ResourceMeta {
	std := make([]*ResourceMeta, len(cells))
	for i := range std {
		std[i] = cells[i].(*ResourceMeta)
	}
	return std
}
