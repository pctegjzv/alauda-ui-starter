package other

import (
	"alauda-ui-starter/src/backend/api"
	"alauda-ui-starter/src/backend/resource/event"
	"encoding/json"
	"fmt"
	coreV1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
)

type OtherResourceDetail struct {
	ObjectMeta api.ObjectMeta   `json:"objectMeta"`
	TypeMeta   ResourceTypeMeta `json:"typeMeta"`
	Data       string           `json:"data"`
	Events     []coreV1.Event   `json:"events"`
}

func GetResourceDetail(dyclient *dynamic.Client, k8sclient kubernetes.Interface, resource *v1.APIResource, namespace string, name string) (*OtherResourceDetail, error) {
	r, err := dyclient.Resource(resource, namespace).Get(name, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	result := OtherResourceDetail{
		ObjectMeta: api.ObjectMeta{
			Name:              r.GetName(),
			Namespace:         r.GetNamespace(),
			Labels:            r.GetLabels(),
			Annotations:       r.GetAnnotations(),
			CreationTimestamp: r.GetCreationTimestamp(),
		},
		TypeMeta: ResourceTypeMeta{
			Name:         r.GetName(),
			Kind:         r.GetKind(),
			GroupVersion: r.GetAPIVersion(),
		},
	}
	raw, err := r.MarshalJSON()
	if err != nil {
		return nil, err
	}
	result.Data = string(raw)

	if r.GetUID() != "" {
		events, err := event.GetEventsByUid(k8sclient, string(r.GetUID()))
		if err != nil {
			return nil, err
		}
		result.Events = events
	}
	return &result, nil
}

func DeleteResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string) error {
	err := client.Resource(resource, namespace).Delete(name, &v1.DeleteOptions{})
	return err
}

func UpdateResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string, payload *unstructured.Unstructured) error {
	if payload.GetName() != name || payload.GetNamespace() != namespace {
		return fmt.Errorf("can not update name and namespace field")
	}

	_, err := client.Resource(resource, namespace).Update(payload)
	return err
}

func PatchResource(client *dynamic.Client, resource *v1.APIResource, namespace string, name string, field string, payload *FieldPayload) error {
	fieldPayloadString, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	rc := client.Resource(resource, namespace)
	patchPayloadTemplate :=
		`[{
        "op": "replace",
        "path": "/metadata/%s",
        "value": %s
    }]`
	patchPayload := fmt.Sprintf(patchPayloadTemplate, field, fieldPayloadString)

	_, err = rc.Patch(name, types.JSONPatchType, []byte(patchPayload))
	return err
}
