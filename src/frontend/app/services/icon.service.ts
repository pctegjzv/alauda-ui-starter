import { Inject, Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

/**
 * A map of MatIcon name to SVG path. The following will be
 * registered into MatIcon, so to be used with
 * <mat-icon svgIcon="iconName"></mat-icon>
 *
 * TODO: use a script to generate icons automatically.
 */
export const SvgIcons = {
  'app-logo': 'app-logo.svg',
  'app-logo-text': 'app-logo-text.svg',
  // Add more here?
};

@Injectable()
export class IconService {
  private readonly iconPath = 'assets/icons';

  constructor(
    @Inject(MatIconRegistry) private iconRegistry_: MatIconRegistry,
    @Inject(DomSanitizer) private sanitizer_: DomSanitizer,
  ) {
    /**
     * Register all SVG icon images. Once registered, your svg icon can be used
     * with <mat-icon svgIcon="iconName"></mat-icon> element
     */
    Object.entries(SvgIcons).forEach(([name, path]) => {
      iconRegistry_.addSvgIcon(
        name,
        sanitizer_.bypassSecurityTrustResourceUrl(
          `${this.iconPath}/${path}`,
        ),
      );
    });
  }
}
