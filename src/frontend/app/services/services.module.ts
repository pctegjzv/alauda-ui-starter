import { NgModule, Inject, Optional, SkipSelf } from '@angular/core';
import { IconService } from './icon.service';

/**
 * Services in Angular App is static and singleton over the whole system,
 * so we will import them into the root module.
 *
 * This is to replace the Core module, which we believe is somewhat duplicates with the
 * purpose of global service module.
 */
@NgModule({
  providers: [IconService]
})
export class ServicesModule {
  /* Make sure ServicesModule is imported only by one NgModule the RootModule */
  constructor(
    @Inject(ServicesModule)
    @Optional()
    @SkipSelf()
    parentModule: ServicesModule,
    iconService: IconService,
  ) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import only in AppModule.',
      );
    }
  }
}
