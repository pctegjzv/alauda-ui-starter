import {
  ModuleWithProviders,
  NgModule,
  Type,
  getPlatform,
} from '@angular/core';
import {
  TranslateLoader,
  TranslateService as NgxTranslateService,
  TranslateModule as NgxTranslateModule,
} from '@ngx-translate/core';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Universal translation file
import universalI18n from './i18n';
import { Language, LanguageBlob } from './translate.types';
import { TranslateService } from './translate.service';

export class CoreTranslateLoader implements TranslateLoader {
  getTranslation(lang: Language): Observable<any> {
    return of((universalI18n as any)[lang]);
  }
}

/**
 * Wraps translate module for ease of use. This is to be added to shared module.
 */
@NgModule({
  exports: [NgxTranslateModule],
})
export class TranslateModule {}

@NgModule({
  imports: [
    NgxTranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CoreTranslateLoader,
      },
    }),
  ],
  providers: [TranslateService],
  exports: [NgxTranslateModule],
})
export class GlobalTranslateModule {}
