export const en = {
  hello: 'Hello',
};

export const zh = {
  hello: '你好',
};

export default {
  en,
  zh,
};
