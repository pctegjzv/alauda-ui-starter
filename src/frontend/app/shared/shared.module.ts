import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';
import { AuiModule } from 'alauda-ui';

import { TranslateModule } from '../translate';
import { ComponentsModule } from './components/components.module';
import { PipesModule } from './pipes/pipes.module';
import { DirectivesModule } from './directives/directives.module';

const EXPORTABLE_IMPORTS = [
  // Vendor modules:
  CommonModule,
  TranslateModule,

  // Material imports
  MatIconModule,

  // AUI imports
  AuiModule,

  // App shared modules:
  ComponentsModule,
  PipesModule,
  DirectivesModule,
];

@NgModule({
  imports: [
    ...EXPORTABLE_IMPORTS
  ],
  exports: [...EXPORTABLE_IMPORTS],
  declarations: []
})
export class SharedModule { }
