import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';

import i18n from './i18n';

import { TranslateModule, TranslateService } from '../../translate';
import { OthersRoutingModule } from './others-routing.module';
import { OthersPageComponent } from './others-page.component';

@NgModule({
  imports: [SharedModule, OthersRoutingModule],
  declarations: [OthersPageComponent],
})
export class OthersModule {
  // For lazy loaded modules, you need to manually load the language pack by your self.
  constructor(translate: TranslateService) {
    translate.setTranslations('others', i18n);
  }
}
