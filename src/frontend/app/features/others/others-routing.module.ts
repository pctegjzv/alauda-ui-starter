import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { OthersPageComponent } from './others-page.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: '', component: OthersPageComponent },
    ]),
  ],
  exports: [RouterModule]
})
export class OthersRoutingModule {}
