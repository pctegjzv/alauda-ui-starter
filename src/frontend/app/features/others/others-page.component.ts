import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { map, share } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { TranslateService } from '../../translate';

@Component({
  templateUrl: './others-page.component.html',
})
export class OthersPageComponent implements OnInit {
  rawList$: Observable<any[]>;
  list$: Observable<any>;

  sortBy$ = new BehaviorSubject({
    active: 'creationTimestamp',
    direction: 'asc',
  });

  constructor(private http: HttpClient) {}

  ngOnInit() {
    // Demos how you can translate messages with stream.
    this.rawList$ = this.http
      .get<any>('api/v1/others')
      .pipe(
        map(res => res.resources.map((item: any) => this.normalize(item))),
        share(),
      );

    this.list$ = combineLatest(this.rawList$, this.sortBy$).pipe(
      map(([rawList, sortBy]) => this.sortColumn(rawList, sortBy)),
    );
  }

  sortData(event: any) {
    this.sortBy$.next(event);
  }

  private sortColumn(
    rawList: any[],
    sortBy: { active: string; direction: string },
  ) {
    return rawList.sort(
      (a, b) =>
        (b[sortBy.active]
          ? b[sortBy.active].localeCompare(a[sortBy.active])
          : 1) * (sortBy.direction === 'asc' ? 1 : -1),
    );
  }

  private normalize(item: any) {
    return { ...item.typeMeta, ...item.objectMeta };
  }
}
