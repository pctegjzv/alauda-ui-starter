import { Component, OnInit } from '@angular/core';

import { TranslateService } from '../translate';

@Component({
  selector: 'app-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['layout.component.scss']
})

export class LayoutComponent implements OnInit {
  ngOnInit() {
  }
  constructor(private translate: TranslateService) {

  }
  toggleLange() {
    this.translate.changeLanguage(this.translate.otherLang);
  }
}
