import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  LayoutModule as AuiLayoutModule,
  NavModule as AuiNavModule,
} from 'alauda-ui';

import { SharedModule } from '../shared';
import { LayoutComponent } from './layout.component';

@NgModule({
  imports: [SharedModule, AuiLayoutModule, AuiNavModule, RouterModule],
  declarations: [LayoutComponent],
  exports: [LayoutComponent],
})
export class LayoutModule {}
